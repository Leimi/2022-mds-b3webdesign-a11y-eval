# Évaluation

## Partie 1 : audit

Réaliser un audit de l'état de l'accessiblité de la page donnée.

Fournir un fichier dans un format tableur (fichier excel ou libreoffice) listant les erreurs d'accessibilité repérées. Pour chaque erreur, indiquer :

- sa criticité : est-ce une erreur importante à résoudre ou non ? (avoir une colonne "critique ?" où on notera "oui" ou "non" par exemple)
- pourquoi est-ce une erreur ? Pourquoi doit-on résoudre ce problème ? (avoir une colonne "raison". Par exemple une ligne aura dans cette colonne "le ratio de couleur trop faible empêchera une personne daltonienne de comprendre correctement l'information").

Il n'est pas nécessaire de détailler dans l'audit les recommandations de corrections à effectuer. Car vous ferez vous-même les corrections dans la partie 2.

Il y a de nombreuses erreurs, certaines plus critiques que d'autres. Pensez utilisateur et focalisez-vous en 1er sur les tests importants afin de déceler les erreurs majeurs avant tout. Car si vous n'avez pas le temps de tout faire, il vaut mieux avoir relevé ce qui va le plus impacter les utilisateurs.

## Partie 2 : corrections

Effectuez les corrections sur les erreurs relevées lors de votre audit en modifiant les fichiers HTML, CSS et JavaScript.

Si vous voyez que vous commencez à corriger des choses non relevées dans l'audit, il y a un problème. Votre audit est sensé être exhaustif. Et vos corrections sont sensées être une mise en pratique de l'audit.

Note : exceptionnellement, les potentielles erreurs que vous avez relevées sur le carousel d'images ne sont pas à corriger. 

## Barème

L'audit est le point le plus important. Il faut s'attendre à avoir deux tiers de la note sur l'audit et un tiers sur les corrections.

Les erreurs critiques, les plus importantes, valent plus de point. Gérez votre temps en fonction.

## Ce qui est autorisé et ce qui ne l'est pas

- Cette évaluation est **individuelle**

- Vous avez le droit de consulter vos cours, les précédents exercices, et plus globalement, Internet. Tant que vous faites ça en autonomie, pas de problème. Il est vivement conseillé de vous baser sur les cours et le RGAA.

- Vous pouvez utiliser un lecteur d'écran dans un casque ou des écouteurs. Ce n'est pas nécessaire pour réussir l'évaluation, mais ça peut être une bonne aide.

## Rendu

Pour me rendre votre travail :

- renommez le dossier de travail donné avec votre nom et votre prénom. Exemple : PELLETIER-Emmanuel
- modifiez directement les fichiers html/css/js
- ajoutez dans ce dossier votre fichier excel ou libreoffice de l'audit
- créez une archive .zip de ce dossier avec un nom identique au dossier.

Voici l'aborescence que devrait avoir votre rendu, si à tout hasard si vous vous appeliez Emmanuel Pelletier :

PELLETIER-Emmanuel.zip
  > PELLETIER-Emmanuel
    > index.html
	> style.css
	> script.js
	> audit.xls 
	> images/...
	> swiper.css
	> swiper.js
	> consignes.md

Envoyez moi ce fichier zip en privé via Teams.
